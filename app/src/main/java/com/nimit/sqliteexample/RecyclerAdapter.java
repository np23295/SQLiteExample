package com.nimit.sqliteexample;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 * Created by Nimit on 02-10-2017.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    private final ArrayList<Person> mContactList;
    private Context mContext;

    public RecyclerAdapter(Context context, ArrayList<Person> mContactList) {
        this.mContext = context;
        this.mContactList = mContactList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyler_content, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String value = mContactList.get(position).getName() + " " + mContactList.get(position).getSurname();
        holder.mText.setText(value);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return mContactList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        AppCompatTextView mText;
        public ViewHolder(View itemView) {
            super(itemView);
            mText = (AppCompatTextView) itemView.findViewById(R.id.contactDetails);
        }
    }
}
