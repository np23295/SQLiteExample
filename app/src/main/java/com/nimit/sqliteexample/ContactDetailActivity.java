package com.nimit.sqliteexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;

public class ContactDetailActivity extends AppCompatActivity {
    private AppCompatEditText mName,mSurname;
    private AppCompatButton mSubmitButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_detail);
        mName = (AppCompatEditText) findViewById(R.id.name);
        mSurname = (AppCompatEditText) findViewById(R.id.surname);
        mSubmitButton = (AppCompatButton) findViewById(R.id.submit);
//        final Person person = getIntent().getExtras().getParcelable("contact");
//        if (person != null){
//            mSubmitButton.setText("Update");
//        }else{
//            mSubmitButton.setText("Create");
//        }
        mSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSubmitButton.getText().equals("Update")){
//                    Person model = new Person();
//                    model.setId(person.getId());
//                    model.setName(String.valueOf(mName.getText()));
//                    model.setSurname(String.valueOf(mSurname.getText()));
//                    DatabaseHelper db = new DatabaseHelper(getBaseContext());
//                    db.updateContact(model);

                } else if(mSubmitButton.getText().equals("Create")){
                    Person model = new Person();
                    model.setName(String.valueOf(mName.getText()));
                    model.setSurname(String.valueOf(mSurname.getText()));
                    DatabaseHelper db = new DatabaseHelper(getBaseContext());
                    db.createContact(model);
                }
            }
        });
    }
}
