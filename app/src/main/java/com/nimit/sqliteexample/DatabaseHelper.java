package com.nimit.sqliteexample;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

import static android.media.MediaFormat.KEY_DURATION;

/**
 * Created by Nimit on 01-10-2017.
 */

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 7;
    private static final String DATABASE_NAME = "sampleDb";
    private String TAG = getClass().getName();
    private String TABLE = "detail";
    private String KEY_ID = "id";
    private String KEY_NAME = "name";
    private String KEY_SURNAME = "surname";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_PERSON_DETAILS = "CREATE TABLE " + TABLE + "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT," + KEY_SURNAME + " TEXT" + ")";
        Log.d(TAG, "onCreate: QUERY: " + CREATE_PERSON_DETAILS);
        sqLiteDatabase.execSQL(CREATE_PERSON_DETAILS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE);
        onCreate(sqLiteDatabase);
    }

    public void createContact(Person model) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(KEY_NAME, model.getName());
        cv.put(KEY_SURNAME, model.getSurname());
        Log.d(TAG, "createContact: "+db.insert(TABLE,null,cv));
    }

    public ArrayList<Person> getAllData(){
        ArrayList<Person> mList=new ArrayList<>();
        SQLiteDatabase db =getReadableDatabase();
        Cursor res=db.rawQuery("SELECT * FROM "+TABLE,null);
        res.moveToFirst();

        while(!res.isAfterLast()){
            Person person = new Person();
            person.setId(res.getColumnIndex(KEY_ID));
            person.setName(String.valueOf(res.getString(res.getColumnIndex(KEY_NAME))));
            person.setSurname(String.valueOf(res.getString(res.getColumnIndex(KEY_SURNAME))));
            mList.add(person);
            res.moveToNext();
        }
        db.close();
        return mList;
    }

    public void updateContact(Person model) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();

        cv.put(KEY_NAME, model.getName());
        cv.put(KEY_SURNAME, model.getSurname());
        db.update(TABLE,cv,KEY_ID + " = ?",new String[]{String.valueOf(model.getId())});
    }
}
